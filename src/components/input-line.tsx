/** ===== Input Line =====
 *
 * Input Line Component
 *
 */

// Qwik
import { component$ } from "@builder.io/qwik";
import { input } from "~/pandacss/recipes/atomic/input.recipe";
// Types
import type { InputStructure } from "~/plugins/db-adapter/types";
// Style
import { css } from "../../styled-system/css";
import { flex } from "../../styled-system/patterns";

export interface inputLineProps {
  input: InputStructure;
  label: string;
  key: number;
}

// Style
const inputName = css({
  textTransform: "capitalize",
  w: "20%",
});

const checkHidden = (
  label: string,
  inputStruc: InputStructure,
  key: number
) => {
  let $input = (
    <input
      class={`${input()}`}
      id={inputStruc.name}
      name={inputStruc.name}
      type={inputStruc.type}
      value={inputStruc.value}
      key={key}
    />
  );

  if (inputStruc.required) {
    $input = (
      <input
        class={`${input()}`}
        id={inputStruc.name}
        name={inputStruc.name}
        type={inputStruc.type}
        value={inputStruc.value}
        key={key}
        required
      />
    );
  }

  if (inputStruc.type === "hidden") return $input;
  else {
    return (
      <label class={`${flex({ align: "center", gap: "1rem" })}`}>
        <span class={`${inputName}`}>{label}</span>
        {$input}
      </label>
    );
  }
};

export default component$<inputLineProps>(
  ({
    input: { name, type = "text", required = false, value = "" },
    label,
    key,
  }) => {
    return checkHidden(label, { name, type, required, value }, key);
  }
);
