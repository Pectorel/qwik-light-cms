/** ===== Aside Menu =====
 *
 * CMS Aside Menu containing all
 * the links provided in the cms-menu Table
 *
 */

// Qwik
import { component$, useTask$ } from "@builder.io/qwik";
import { Link } from "@builder.io/qwik-city";
// DB-Adapter
import type { CMSCategory } from "~/plugins/db-adapter/mongoose/Schema/cms-menu.schema";
// Font-Awesome
import {
  faMask,
  faDumbbell,
  faBookAtlas,
  faGamepad,
  faCodeCompare,
  faPalette,
  faUser,
  faBell,
  faUsers,
  faShieldHalved,
} from "@fortawesome/free-solid-svg-icons";
import { FaIcon } from "qwik-fontawesome";
import type { IconDefinition } from "@fortawesome/free-regular-svg-icons";
// Style
import { css } from "../../../styled-system/css";
import { flex } from "../../../styled-system/patterns";

const asideWidth = "300px";

// PandaCSS
const asideMenu = css({
  h: "calc(100vh - 100px)",
  bg: "purple.200",
  overflowX: "hidden",
  w: asideWidth,
  transition: "width .2s ease-out",
  "&.closed": {
    w: 0,
  },
});

const asideCategory = css({
  w: asideWidth,
});

const categoryTitle = css({
  bg: "rgba(0 0 0 / 5%)",
  textTransform: "uppercase",
  p: ".8rem 1rem .6rem",
  borderBottom: "1px solid rgba(0 0 0 / 30%)",
});

const asideLink = css({
  p: ".875rem 2rem .75rem",
  borderBottom: "1px solid rgba(0 0 0 /30%)",
  _hover: {
    color: "purple.50",
  },
});

const icon = css({
  fontSize: "1.25rem",
  marginRight: "1rem",
  w: "32px",
  textAlign: "left",
});

// Types
interface AsideProps {
  menus: Array<CMSCategory>;
  class?: string;
}

export const icons = [
  faMask,
  faDumbbell,
  faBookAtlas,
  faGamepad,
  faCodeCompare,
  faPalette,
  faUser,
  faBell,
  faUsers,
  faShieldHalved,
];

// Component
export default component$<AsideProps>(({ menus, ...props }) => {
  useTask$(() => {
    let i = 0;
    for (const categ of menus) {
      for (const menu of categ.menus) {
        menu.icon = icons[i];
        i++;
      }
    }
  });

  return (
    <aside class={`${asideMenu} ${props.class}`}>
      {menus.map((categ) => (
        <div class={asideCategory}>
          <h2 class={categoryTitle}>{categ.label}</h2>
          <div class={flex({ direction: "column" })}>
            {categ.menus.map((menu, key) => (
              <Link
                href={`/cms/${menu.slug}`}
                class={`${asideLink} ${flex({ align: "center" })}`}
                key={key}
              >
                <FaIcon
                  icon={menu.icon as IconDefinition}
                  class={icon}
                  preserveAspectRatio="xMinYMin meet"
                />
                <span>{menu.label}</span>
              </Link>
            ))}
          </div>
        </div>
      ))}
    </aside>
  );
});
