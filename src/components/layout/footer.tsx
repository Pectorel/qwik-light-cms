import { component$ } from "@builder.io/qwik";
import { flex } from "../../../styled-system/patterns";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import { FaIcon } from "qwik-fontawesome";
import { wrapper } from "../../../styled-system/recipes";
import { css } from "../../../styled-system/css";

const footer = css({
  pos: "relative",
  paddingTop: "1.25rem",
  paddingBottom: ".85rem",
  fontSize: ".875rem",
  textAlign: "center",
  fontWeight: "300",
  color: "rgba(229 229 229 / 70%)",
  _after: {
    content: '""',
    pos: "absolute",
    h: "1px",
    left: 0,
    right: 0,
    top: 0,
    bg: "rgba(255 255 255 / 40%)",
  },
});

const footerText = css({
  flex: 1,
});

const icon = css({
  fontSize: "1.25rem!",
});

export default component$(() => {
  return (
    <footer
      class={`${flex({ justify: "space-between" })} ${wrapper()} ${footer}`}
    >
      <span class={`${footerText}`}>
        Qwik Light CMS is an open source project made by Pectorel and is not
        affiliated with the Qwik Team or Builder.io
      </span>
      <div>
        <a
          href="https://gitlab.com/Pectorel/qwik-light-cms"
          target="_blank"
          rel="noreferrer noopener"
        >
          <FaIcon class={`${icon}`} icon={faGitlab} />
        </a>
      </div>
    </footer>
  );
});
