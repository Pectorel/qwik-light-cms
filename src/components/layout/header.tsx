// Qwik
import { $, component$, qrl } from "@builder.io/qwik";
import { server$, useNavigate } from "@builder.io/qwik-city";
// Db-Adapter
import type { SessionAdmin } from "~/plugins/db-adapter/types";
// Font-Awesome
import {
  faBars,
  faArrowRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";
import { faBell } from "@fortawesome/free-regular-svg-icons";
import { FaIcon } from "qwik-fontawesome";
// Style
import { flex } from "../../../styled-system/patterns";
import { css } from "../../../styled-system/css";
// Imgs
import ImgMenuLogo from "~/assets/static/img/qwick-cms-logo.png?jsx";
import ImgProfilePlaceholder from "~/assets/static/img/user-placeholder.png?jsx";
// Node Modules
import { existsSync } from "node:fs";

const header = css({
  pos: "relative",
  bg: "purple.200",
  p: "1.25rem 2rem",
  boxShadow: "0 3px 3px rgba(0 0 0 / 25%)",
  zIndex: "4",
});

const headerLeft = css({
  fontSize: "2rem",
  "& h1": {
    fontWeight: 600,
  },
});

const icon = css({
  pos: "relative",
  display: "block",
  paddingRight: "1.5rem",
  marginRight: ".7rem",
  cursor: "pointer",
  _after: {
    content: '""',
    pos: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    w: "1px",
    bg: "rgba(0 0 0 / 40%)",
  },
});

const headerRight = css({
  fontSize: "1.25rem",
});

const linkIcon = css({
  cursor: "pointer",
});

const notifIcon = css({
  pos: "relative",
  paddingRight: "1.25rem",
  _after: {
    content: '""',
    pos: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    w: "1px",
    bg: "rgba(0 0 0 / 40%)",
  },
});

const profile = css({
  fontSize: "1rem",
  textAlign: "right",
});

const profileImg = css({
  w: "60px",
  h: "60px",
  borderRadius: "50%",
  overflow: "hidden",
  bg: "purple.400",
  boxShadow: "inset 0 0 6px rgba(0 0 0 / 30%)",
});

interface HeaderProps {
  admin: SessionAdmin;
}

export const toggleAside = $(() => {
  document.querySelector("aside")?.classList.toggle("closed");
});

export const setImage = server$((id: string) => {
  let res = <ImgProfilePlaceholder class={profileImg} />;
  const src = `/admins/${id}/profile.jpg`;
  if (existsSync(`./public/${src}`)) {
    res = <img src={src} alt="Profile Picture" class={profileImg} />;
  }
  return res;
});

export default component$<HeaderProps>(({ admin }) => {
  const location: string = "Dashboard";
  const nav = useNavigate();

  const adminLogout = $(async () => {
    await nav("/cms/logout");
  });

  return (
    <header
      class={`${flex({ align: "center", justify: "space-between" })} ${header}`}
    >
      <div class={`${flex({ align: "center", gap: ".8rem" })} ${headerLeft}`}>
        <span class={icon} onClick$={toggleAside}>
          <FaIcon icon={faBars} />
        </span>
        <ImgMenuLogo />
        <h1>{location}</h1>
      </div>
      <div
        class={`${headerRight} ${flex({
          align: "center",
          gap: "1.25rem",
        })}`}
      >
        <span class={`${linkIcon} ${notifIcon}`}>
          <FaIcon icon={faBell} />
        </span>

        <div class={`${flex({ align: "center", gap: ".75rem" })} ${profile}`}>
          <p>
            {admin.name.first}
            <br />
            {admin.name.last}
          </p>
          {setImage(admin._id)}
        </div>
        <FaIcon
          class={`${linkIcon}`}
          icon={faArrowRightFromBracket}
          onClick$={adminLogout}
        ></FaIcon>
      </div>
    </header>
  );
});
