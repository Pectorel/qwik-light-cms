/** ===== CMS Form =====
 *
 * Contains the Form structure
 *
 */

import { component$, h } from "@builder.io/qwik";
import type { InputStructure } from "~/plugins/db-adapter/types";
import InputLine from "~/components/input-line";
import CmsForm from "~/components/cms-form";
// Types
import JSX = h.JSX;
// Style
import { css } from "../../styled-system/css";
import { flex } from "../../styled-system/patterns";

interface CmsFormProps {
  inputs: Array<InputStructure> | undefined;
  label?: string | undefined;
}

// Style
const formContainer = css({
  p: "1rem 2rem",
  bg: "purple.400",
  boxShadow: "2px 2px 4px rgba(0 0 0 / 35%), 1px 1px 6px rgba(0 0 0 / 15%)",

  "& > div": {
    bg: "purple.300",
  },
});

const formTitle = css({
  fontSize: "1.5rem",
  fontWeight: 600,
  textTransform: "capitalize",
});

export default component$<CmsFormProps>(({ inputs, label }) => {
  const sections: Array<JSX.Element> = [];

  const checkInputStructure = (input: InputStructure, key: number) => {
    if (input.type == "section") {
      sections.push(
        <CmsForm inputs={input.objProperties} label={input.name} />
      );
      return false;
    }

    return (
      <InputLine
        label={input.name}
        input={input}
        /* @ts-ignore */
        key={`${input.name}_${key}`}
      />
    );
  };
  return (
    <div
      class={`${label ? formContainer : ""} ${flex({
        direction: "column",
        gap: "1.5rem",
        align: "stretch",
      })}`}
    >
      {label ? <h3 class={formTitle}>{label}</h3> : ""}
      {inputs?.map((input, key) => checkInputStructure(input, key))}
      {sections.map((input) => input)}
    </div>
  );
});
