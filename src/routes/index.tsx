// Qwik
import { component$ } from "@builder.io/qwik";
import type { DocumentHead } from "@builder.io/qwik-city";
import { Form, routeAction$, useNavigate } from "@builder.io/qwik-city";
import type { RequestHandler } from "@builder.io/qwik-city";
// Components
import Footer from "~/components/layout/footer";
import ImgLogo from "~/assets/static/img/qwik-logo.png?jsx";
// DB Adapter
import DBAdapter from "~/plugins/db-adapter/handler";
import type { Response } from "~/plugins/db-adapter/db-response";
import DBResponse from "~/plugins/db-adapter/db-response";
// Auth
import { isAuthentified } from "~/plugins/auth/auth";
// Utils
import { setExpireDays } from "~/utils/cookie-utils";
// Styles
import { flex } from "../../styled-system/patterns";
import { css } from "../../styled-system/css";
import { input } from "~/pandacss/recipes/atomic/input.recipe";
import { button } from "~/pandacss/recipes/atomic/button.recipe";
import { showNotif } from "~/utils/notify";

const fullpage = css({
  height: "100vh",
});

const main = css({
  w: "100%",
  flex: 1,
});

const index = css({
  h: "100%",
  position: "relative",
});

const title = css({
  fontSize: "4rem",
  fontWeight: 500,
});

const logo = css({
  w: "120px",
});

const form = css({
  w: "50%",
  maxWidth: "300px",
  "& > input:not(:first-child), & > button:not(:first-child)": {
    marginTop: "1.25rem",
  },
  "& > button": {
    m: "0 auto",
  },
});

export const onGet: RequestHandler = async ({ cacheControl }) => {
  // Control caching for this request for best performance and to reduce hosting costs:
  // https://qwik.builder.io/docs/caching/
  cacheControl({
    // Always serve a cached response by default, up to a week stale
    staleWhileRevalidate: 60 * 60 * 24 * 7,
    // Max once every 5 seconds, revalidate on the server to get a fresh version of this page
    maxAge: 5,
  });
};

export const onRequest: RequestHandler = async ({
  next,
  cookie,
  env,
  redirect,
}) => {
  const authentified = await isAuthentified(cookie, env);
  // If already authentified then redirect to cms dashboard
  if (authentified.success) {
    throw redirect(302, "/cms");
  }
  await next();
};

export const useLoginAction = routeAction$(async (data, { env, cookie }) => {
  // Db Connexion
  const dbInstance = await DBAdapter.getInstance(env);

  const resInstance = new DBResponse(
    "Database is not Connected, check your environment variables"
  );
  let res = resInstance.response;

  if (!dbInstance.connexion.isConnected()) return res;

  // Checking Auth
  res = (
    await dbInstance.auth.auth(
      {
        login: data.login as string,
        password: data.password as string,
      },
      env
    )
  ).response;

  // We set cookie if logged-in
  if (res.status.success) {
    // @ts-ignore
    cookie.set("Auth Token", res.data?.admin.token, {
      expires: setExpireDays(30),
    });
  }

  return res;
});

const loggedIn = (response: Response) => {
  // eslint-disable-next-line qwik/use-method-usage
  const nav = useNavigate();

  setTimeout(() => {
    nav("/cms").then();
  }, 2000);

  return showNotif(response.status.message);
};

export default component$(() => {
  const loginAction = useLoginAction();

  return (
    <div
      class={`${flex({
        direction: "column",
        align: "center",
        justify: "space-between",
      })} ${fullpage}`}
    >
      <main class={`${main}`}>
        {loginAction.value?.status?.success
          ? loggedIn(loginAction.value as Response)
          : showNotif(loginAction.value?.status?.message as string)}
        <div
          class={`${index} ${flex({
            direction: "column",
            align: "center",
            justify: "center",
            gap: "1.5rem",
          })}`}
        >
          <h1 class={`${flex({ align: "center", gap: "1.25rem" })} ${title}`}>
            <ImgLogo class={`${logo}`} alt={"Qwik"} />
            Light CMS
          </h1>

          <Form action={loginAction} class={`${form}`}>
            <input
              class={input()}
              type="text"
              name="login"
              placeholder="Login"
            />
            <input
              class={input()}
              type="password"
              name="password"
              placeholder="Password"
            />
            <button class={`${button()}`} type="submit">
              Sign In
            </button>
          </Form>
        </div>
      </main>
      <Footer />
    </div>
  );
});

export const head: DocumentHead = {
  title: "Qwik Light CMS - Connexion",
  meta: [
    {
      name: "description",
      content: "Connexion page of Qwik Light CMS",
    },
  ],
};
