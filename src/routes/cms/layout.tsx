// Qwik
import { component$, Slot } from "@builder.io/qwik";
import type { RequestHandler } from "@builder.io/qwik-city";
import { routeLoader$ } from "@builder.io/qwik-city";
// Components
import Header from "~/components/layout/header";
import Footer from "~/components/layout/footer";
import AsideMenu from "~/components/layout/aside-menu";
// Db-Adapter
import type { SessionAdmin } from "~/plugins/db-adapter/types";
// Auth
import { isAuthentified } from "~/plugins/auth/auth";
// Style
import { css } from "../../../styled-system/css";
import { flex } from "../../../styled-system/patterns";
/**
 *
 * Check if User is already Authentified
 *
 * @param next
 * @param cookie
 * @param error
 * @param env
 * @param sharedMap
 */
export const onRequest: RequestHandler = async ({
  next,
  cookie,
  error,
  env,
  sharedMap,
}) => {
  const authentified = await isAuthentified(cookie, env);

  if (!authentified.success) {
    cookie.delete("Auth Token", { path: "/" });
    const err = authentified.error;
    throw error(err?.errCode as any, err?.errMessage as string);
  }

  sharedMap.set("admin", authentified.content.decodedToken);
  sharedMap.set("DbInstance", authentified.content.dbInstance);
  await next();
};

/**
 * Get the Admin users details based on the client "Auth Token"
 */
export const useAdminDetails = routeLoader$(({ sharedMap }) => {
  return sharedMap.get("admin") as SessionAdmin;
});

/**
 * Get The CMS Menu Categories and Links
 */
export const useMenuDetails = routeLoader$(async ({ sharedMap }) => {
  const dbInstance = sharedMap.get("DbInstance");
  return await dbInstance.cms.getMenu(sharedMap.get("admin") as SessionAdmin);
});

// Style
const content = css({
  flex: "1",
  height: "calc(100vh - 100px)",
  overflowY: "auto",
  "& > footer": {
    maxWidth: "none",
  },
});

const main = css({
  p: "3rem",
});

export default component$(() => {
  const admin = useAdminDetails().value;
  const menus = useMenuDetails().value;
  return (
    <>
      <Header admin={admin} />
      <div class={flex({ justify: "space-between" })}>
        {/* @ts-ignore */}
        <AsideMenu menus={menus} />
        <div class={content}>
          <main class={main}>
            <Slot />
          </main>
          <Footer />
        </div>
      </div>
    </>
  );
});
