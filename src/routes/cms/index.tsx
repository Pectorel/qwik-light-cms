import { component$ } from "@builder.io/qwik";

export default component$(() => {
  return (
    <section>
      <h1>Hello World !</h1>
    </section>
  );
});
