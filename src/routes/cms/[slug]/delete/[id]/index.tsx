import type { RequestHandler } from "@builder.io/qwik-city";
import DBAdapter from "~/plugins/db-adapter/handler";
import DebugPrint from "~/utils/debug-print";

export const debugPrint = new DebugPrint({ prefix: "[DELETE]" });
export const onRequest: RequestHandler = async ({
  params,
  env,
  error,
  redirect,
}) => {
  try {
    const dbInstance = await DBAdapter.getInstance(env);
    const collection_name = `${params.slug}s`;
    const res = await dbInstance.cms.deleteOne(collection_name, params.id, env);

    debugPrint.success("Records Deleted from DB :");
    debugPrint.dir(res);
  } catch (err: any) {
    throw error(404, "Resource to delete not found");
  }
  throw redirect(302, "/");
};
