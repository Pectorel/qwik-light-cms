// Qwik
import { $, component$ } from "@builder.io/qwik";
import { Form, routeAction$, routeLoader$ } from "@builder.io/qwik-city";
// Components
import CmsForm from "~/components/cms-form";
// Plugins
import DBAdapter from "~/plugins/db-adapter/handler";
import DebugPrint from "~/utils/debug-print";
// Style
import { css } from "../../../../../styled-system/css";
import { flex } from "../../../../../styled-system/patterns";
import { button } from "~/pandacss/recipes/atomic/button.recipe";

const debugPrint = new DebugPrint({ prefix: "[CREATE]" });

export const useFormDetails = routeLoader$(async ({ env, params }) => {
  // Getting the current structure of page with validator
  const dbInstance = await DBAdapter.getInstance(env);
  const collection_name = `${params.slug}s`;

  const dataStructure = await dbInstance.cms.getDataStructure(
    collection_name,
    env
  );

  // We Add Custom Input for FormAction
  dataStructure.push({
    name: "table_name",
    type: "hidden",
    value: collection_name,
  });

  debugPrint.dir(dataStructure);
  debugPrint.dir(dataStructure[1]);

  return dataStructure;
});

export const useAddAction = routeAction$(async (data, { env, redirect }) => {
  const dbInstance = await DBAdapter.getInstance(env);
  const collection_name: string = data.table_name as string;
  delete data.table_name;

  try {
    const res = await dbInstance.cms.insert(collection_name, data, env);
    debugPrint.success("Record Created in Database : ");
    debugPrint.dir(res);
  } catch (err: any) {
    debugPrint.error(err);
    return {
      error: true,
      errMessage: "Add failed.",
      errDebug: err.message,
    };
  }
  const slug = collection_name.substring(0, collection_name.length - 1);
  throw redirect(302, `/cms/${slug}`);
});

// Style
const form = css({
  w: "70%",
  m: "0 auto",
});

const btn = css({
  alignSelf: "center",
});

export default component$(() => {
  const dataStructure = useFormDetails();
  const addAction = useAddAction();

  return (
    <section>
      <Form
        action={addAction}
        class={`${form} ${flex({
          direction: "column",
          gap: "1rem",
          align: "stretch",
        })}`}
      >
        <CmsForm inputs={dataStructure.value} />
        <button type={"submit"} class={`${button()} ${btn}`}>
          Add
        </button>
      </Form>
    </section>
  );
});
