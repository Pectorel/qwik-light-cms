import { component$ } from "@builder.io/qwik";
import { Link, routeLoader$, useLocation } from "@builder.io/qwik-city";
import DebugPrint from "~/utils/debug-print";
import DBAdapter from "~/plugins/db-adapter/handler";
import cmsMapping from "~/cms.map";
import {
  faPencil,
  faTrash,
  faPlusSquare,
} from "@fortawesome/free-solid-svg-icons";
import { FaIcon } from "qwik-fontawesome";
import { css } from "../../../../styled-system/css";
import { flex } from "../../../../styled-system/patterns";
export const debugPrint = new DebugPrint({ prefix: "[CMS-Listing]" });

// TODO: Add Confirmation modal for deletion
export const useDataDetails = routeLoader$(async ({ params, env, error }) => {
  // Getting DbInstance
  const dbInstance = await DBAdapter.getInstance(env);
  if (!dbInstance.connexion.isConnected()) {
    throw error(404, "DB Is Not Reachable");
  }
  const collection_name = `${params.slug}s`;
  // We check if db exists
  if (!(await dbInstance.utils.collectionExists(collection_name))) {
    throw error(404, "Collection does not exists");
  }

  debugPrint.info(`Getting data from specified table : ${collection_name}`);
  const dataArray = await dbInstance.cms.find(collection_name, env);

  if (dataArray == null) {
    debugPrint.error("No data found");
  }
  debugPrint.success("All data retrieved");

  return {
    table: collection_name,
    data: dataArray,
  };
});

// Style

const header = css({
  marginBottom: "2rem",
});

const title = css({
  fontSize: "2rem",
  fontWeight: 600,
  textTransform: "capitalize",
});

const line = css({
  w: "100%",
  p: " 1rem 2rem .9rem",
  bg: "purple.300",
  boxShadow: "1px 2px 3px rgba(0 0 0 / 30%)",
});

const addIcon = css({
  fontSize: "2rem",
  pos: "relative",
  top: "2px",
});

export default component$(() => {
  const loc = useLocation();
  const data = useDataDetails();
  return (
    <section>
      {/* TODO: Change Slug by name */}
      <header class={`${flex({ align: "center", gap: "2rem" })} ${header}`}>
        <h2 class={title}>{loc.params.slug}</h2>
        <Link href={"create"} class={`${addIcon}`} key={loc.params.slug}>
          <FaIcon icon={faPlusSquare} />
        </Link>
      </header>

      <div class={`${flex({ direction: "column", gap: "1rem" })}`}>
        {data.value.data.map((row: any, key: number) => (
          <div
            class={`${line} ${flex({
              align: "center",
              justify: "space-between",
            })}`}
            key={key}
          >
            {/* @ts-ignore */}
            <span>{row[cmsMapping[data.value.table].name]}</span>

            <div class={`${flex({ align: "center", gap: "1.875rem" })}`}>
              {/* @ts-ignore */}
              <Link href={`edit/${row[cmsMapping[data.value.table].id]}`}>
                <FaIcon icon={faPencil} />
              </Link>
              {/* @ts-ignore TODO: Change Delete Link With Confirmation Modal */}
              <Link href={`delete/${row[cmsMapping[data.value.table].id]}`}>
                <FaIcon icon={faTrash} />
              </Link>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
});
