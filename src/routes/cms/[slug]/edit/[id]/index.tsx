import { component$ } from "@builder.io/qwik";
import { Form, routeAction$, routeLoader$ } from "@builder.io/qwik-city";
import DBAdapter from "~/plugins/db-adapter/handler";
import { css } from "../../../../../../styled-system/css";
import { button } from "~/pandacss/recipes/atomic/button.recipe";
import DebugPrint from "~/utils/debug-print";
import { showNotif } from "~/utils/notify";
import CmsForm from "~/components/cms-form";

export const debugPrint = new DebugPrint({ prefix: "[EDIT]" });

export const useRowDetails = routeLoader$(async ({ params, env }) => {
  const dbInstance = await DBAdapter.getInstance(env);

  const collection_name = `${params.slug}s`;

  const row = await dbInstance.cms.findOne(collection_name, params.id, env);

  const dataStructure = await dbInstance.cms.getDataStructure(
    collection_name,
    env,
    row
  );

  // We Add Custom Input for FormAction
  dataStructure.push({
    name: "table_name",
    type: "hidden",
    value: collection_name,
  });

  return dataStructure;
});

export const useEditAction = routeAction$(async (data, { env }) => {
  // Update Data
  const dbInstance = await DBAdapter.getInstance(env);
  const collection_name = data.table_name;
  delete data.table;

  debugPrint.info(`Updating ${collection_name} record with this :`);
  debugPrint.dir(data);

  try {
    const res = await dbInstance.cms.update(collection_name, env, data);
    debugPrint.success("Record updated in Database : ");
    debugPrint.dir(res);

    return res;
  } catch (err: any) {
    debugPrint.error(err);
    return {
      error: true,
      errMessage: "Update failed.",
      errDebug: err.message,
    };
  }
});

// Style
const form = css({
  w: "70%",
  m: "auto",
});

const submitBtn = css({
  m: "2rem auto 0",
  w: "30%",
});

const notif = css({
  top: "3rem",
});

export default component$(() => {
  const inputs = useRowDetails();
  const editAction = useEditAction();

  const showError = (err: any) => {
    if (import.meta.env.PUBLIC_DEBUG_MODE === "1") {
      return showNotif(
        [err.errMessage, <br />, "Debug Error : ", err.errDebug],
        notif
      );
    }

    return showNotif(
      `${err.errMessage} Enabled Debug Mode to see error in details`,
      notif
    );
  };

  return (
    <div>
      {editAction.value?.acknowledged
        ? showNotif("Record has been updated in Database", notif)
        : ""}
      {editAction.value?.error ? showError(editAction.value) : ""}
      <Form class={`${form}`} action={editAction}>
        <CmsForm inputs={inputs.value} />
        <button class={`${button()} ${submitBtn}`} type={"submit"}>
          Edit
        </button>
      </Form>
    </div>
  );
});
