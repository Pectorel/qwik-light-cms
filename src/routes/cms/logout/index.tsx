import { RequestHandler } from "@builder.io/qwik-city";

export const onRequest: RequestHandler = ({ cookie, redirect }) => {
  cookie.delete("Auth Token", { path: "/" });
  redirect(302, "/");
};
