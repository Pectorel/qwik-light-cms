import { component$, useErrorBoundary, useTask$ } from "@builder.io/qwik";
import { css } from "../../styled-system/css";
import { flex } from "../../styled-system/patterns";
import { Link } from "@builder.io/qwik-city";

const errorPage = css({
  w: "100vw",
  h: "100vh",
});

const errorStatus = css({
  fontSize: "16rem",
});

const errorMessage = css({
  fontSize: "2rem",
  marginBottom: "5rem",
});

export default component$(() => {
  // eslint-disable-next-line
  let err = useErrorBoundary();

  useTask$(() => {
    // @ts-ignore
    err.error ??= {
      status: 404,
      message: "Error: Page Not Found",
    };
  });

  return (
    <section
      class={`${errorPage} ${flex({
        direction: "column",
        align: "center",
        justify: "center",
        gap: "1rem",
      })}`}
    >
      <h1 class={errorStatus}>{err.error?.status}</h1>
      <p class={errorMessage}>{err.error?.message}</p>
      <Link href={"/"}>Back to homepage</Link>
    </section>
  );
});
