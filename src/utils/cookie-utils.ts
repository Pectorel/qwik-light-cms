/** ===== Cookie Utilities =====
 *
 * Utilities function for cookies
 *
 */

/**
 *
 * Generate expire time based on given days
 *
 * @param days
 */
export const setExpireDays = (days: number) => {
  const d = new Date();
  d.setTime(d.getTime() + days * 24 * 60 * 60 * 1000);
  return d.toUTCString();
};
