/** ===== Notify =====
 *
 * Show / Hide Notification
 *
 */

import type { Cookie, CookieValue } from "@builder.io/qwik-city";
import * as jwt from "jsonwebtoken";
import DebugPrint from "~/utils/debug-print";

export interface JWTError {
  errCode: number;
  errMessage: string;
}

export interface JWTResponse {
  success: boolean;
  content: JWTError | CookieValue | any;
}
export const debugPrint = new DebugPrint({ prefix: "[JWT-Utils]" });

/**
 *
 * checks if Token exists in Cookies
 *
 * @param cookieName
 * @param cookie
 */
export const tokenExists = (cookieName: string, cookie: Cookie) => {
  debugPrint.info("Checking if JWT Cookie Exists");

  //console.log("[JWT-Utils] ℹ️ Checking if JWT Cookie Exists");
  let res: JWTResponse = {
    success: false,
    content: {
      errCode: 1,
      errMessage: "Cookie does not exists",
    },
  };

  const token = cookie.get(cookieName);

  if (token == null) {
    debugPrint.error("Cookie does not exists");
    return res;
  }

  res = {
    success: true,
    content: token,
  };

  debugPrint.success("Cookie does exists");
  return res;
};

export const checkValidity = async (token: CookieValue, env: any) => {
  debugPrint.info("Checking JWT Validity");
  let res: JWTResponse = {
    success: false,
    content: {
      errCode: 2,
      errMessage: "Given token is not valid",
    },
  };

  await jwt.verify(
    token.value,
    env.get("JWT_SECRET"),
    (err: any, decoded: any) => {
      if (err) {
        debugPrint.error("JWT is not valid");
        return res;
      }

      res = {
        success: true,
        content: decoded,
      };
    }
  );

  if (!res.success) return res;

  debugPrint.success("JWT is valid");
  return res;
};

export const getDecodedValue = async (
  cookieName: string,
  cookie: Cookie,
  env: any
) => {
  const token = tokenExists(cookieName, cookie);
  if (!token.success) return null;
  const decoded = await checkValidity(token.content, env);
  if (!decoded.success) return null;
  return decoded.content;
};
