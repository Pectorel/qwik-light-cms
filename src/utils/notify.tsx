/** ===== Notify =====
 *
 * Show / Hide Notification
 *
 */

import { css } from "../../styled-system/css";

const notif = css({
  pos: "absolute",
  left: "50%",
  right: "1rem",
  top: "8rem",
  transform: "translate(-50%, -50%)",
  w: "100%",
  maxWidth: "500px",
  p: "1rem 2rem",
  bg: "purple.600",
  textAlign: "center",
  zIndex: 12,
  opacity: 0,
});


export const showNotif = (message: string | any, classes: string = "") => {
  if (message != null && message.length > 0) {
    setTimeout(() => {
      closeNotif();
    }, 8000);
    setTimeout(() => {
      openNotif();
    }, 200);
    return <div class={`to-open ${notif} ${classes}`}>{message}</div>;
  }

  return "";
};

export const closeNotif = () => {
  document.querySelector(".notif-open")?.classList.remove("notif-open");
};

export const openNotif = () => {
  document.querySelector(".to-open")?.classList.add("notif-open");
};
