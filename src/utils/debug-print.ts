/** ===== Debug Print =====
 *
 * Debug Utility
 * Can be toggled in env files with the PUBLIC_DEBUG_MODE property
 *
 */

export interface DebugPrintProps {
  prefix: string;
}

export default class DebugPrint {
  private readonly _prefix: string;
  constructor(props: DebugPrintProps) {
    this._prefix = props.prefix;
  }

  showMessage(message: string, dir: boolean = false) {
    if (import.meta.env.PUBLIC_DEBUG_MODE === "1") {
      if (dir) console.dir(message);
      else console.log(`[Debug Mode] ${this._prefix} ${message}`);
    }
  }

  error(message: string | any) {
    this.showMessage(`❌  ${message}`);
  }

  success(message: string | any) {
    this.showMessage(`✔️ ${message}`);
  }

  info(message: string | any) {
    this.showMessage(`ℹ️ ${message}`);
  }

  dir(message: string | any) {
    this.showMessage(message, true);
  }
}
