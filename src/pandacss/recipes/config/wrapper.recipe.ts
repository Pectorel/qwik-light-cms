import { defineRecipe } from "@pandacss/dev";

export const wrapperRecipe = defineRecipe({
  name: "wrapper",
  description: "Wrapper Default CSS",
  base: {
    w: "100%",
    maxWidth: "1440px",
    m: "0 auto",
    p: "0 1rem",
  },
});
