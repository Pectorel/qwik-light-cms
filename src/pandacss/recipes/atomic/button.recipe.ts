import { cva } from "../../../../styled-system/css";

export const button = cva({
  base: {
    p: ".7rem 4rem",
    bg: "blue.100",
    color: "#fff",
    display: "block",
    outline: "none",
    fontSize: "1.125rem",
    cursor: "pointer",
    _hover: {
      bg: "blue.200",
    },
  },
});
