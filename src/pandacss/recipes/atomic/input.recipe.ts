import { cva } from "../../../../styled-system/css";

export const input = cva({
  base: {
    p: ".75rem 1rem",
    color: "purple.150",
    display: "block",
    w: "100%",
    outline: "none",
    _placeholder: {
      color: "rgba(63 59 108 / 60%)",
    },
  },
});
