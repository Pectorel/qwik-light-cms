import { ObjectId } from "mongoose";

export interface AdminRow {
  _id: ObjectId;
  role: string;
  name: string;
}

export interface Tokens {
  jwtToken: string | undefined;
  sessionToken: string | undefined;
}

export interface AuthResponse {
  success: boolean;
  error?: {
    errCode: Number;
    errMessage: string;
  };
  content?: any;
}
