import DebugPrint from "~/utils/debug-print";
import { v4 as uuidv4 } from "uuid";
import * as jwt from "jsonwebtoken";
import { getDecodedValue } from "~/utils/jwt-utils";
import type { SessionAdmin } from "~/plugins/db-adapter/types";
import type { AdminRow, Tokens, AuthResponse } from "~/plugins/auth/types";
import DBAdapter from "~/plugins/db-adapter/handler";
import type { Cookie } from "@builder.io/qwik-city";

export const debugPrint = new DebugPrint({ prefix: "[Auth]" });

/**
 * Generates Auth Token
 *
 * @param admin
 * @param env
 */
export const generateAuthToken = (admin: AdminRow, env: any) => {
  const res: Tokens = {
    jwtToken: undefined,
    sessionToken: undefined,
  };

  debugPrint.info("Generating Session Token in uuid");
  // Generating Session Token
  res.sessionToken = uuidv4();
  debugPrint.success(`Admin token generated : ${res.sessionToken}`);

  // We set the cookie token to a JWT for more security
  // This way the connexion token is encrypted and not directly shown on the client
  debugPrint.info("Generating JWT Token to Store on Client");
  res.jwtToken = jwt.sign(
    {
      _id: admin._id.toString(),
      role: admin.role,
      name: admin.name,
      token: res.sessionToken,
    },
    env.get("JWT_SECRET", {
      expiresIn: "30d",
    })
  );

  debugPrint.success(`JSON WEB TOKEN Generated : ${res.jwtToken}`);

  return res;
};

/**
 *
 * Check if User is already Authentified and has a valid JWT cookie
 *
 * @param cookie
 * @param env
 * @param cookieName
 */
export const isAuthentified = async (
  cookie: Cookie,
  env: any,
  cookieName = "Auth Token"
) => {
  const res: AuthResponse = {
    success: false,
    error: {
      errCode: 401,
      errMessage: "Your session has expired, please Sign-In again",
    },
  };

  // Checking Authentification with JWT
  debugPrint.info("Checking User Session");
  const decodedToken = (await getDecodedValue(
    cookieName,
    cookie,
    env
  )) as SessionAdmin | null;

  if (decodedToken == null) {
    debugPrint.error("User Session has expired");
    return res;
  }
  // DBHandler Check Session Function
  const dbInstance = await DBAdapter.getInstance(env);

  // If DB is not connected throw error
  if (!dbInstance.connexion.isConnected()) {
    debugPrint.error("Database Unreachable");
    res.error = {
      errCode: 404,
      errMessage: "Database Unreachable, please try again later",
    };
  }

  // Checking if token is from admin in Database
  const authAdmin = await dbInstance.auth.checkSession(decodedToken);

  if (authAdmin == null) {
    debugPrint.error("User Session has expired");
    return res;
  }

  res.success = true;
  res.error = {
    errCode: 200,
    errMessage: "User is already authentified",
  };
  res.content = {
    dbInstance: dbInstance,
    decodedToken: decodedToken,
  };

  debugPrint.success("User Session is valid");

  return res;
};

export const logout = (cookie: Cookie, cookieName = "Auth Token") => {
  cookie.delete(cookieName);
};
