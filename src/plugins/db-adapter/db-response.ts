/** ===== DBResponse =====
 *
 * Handle the formatting of response from database calls
 *
 */

export interface Status {
  success: boolean;
  message: string;
}

export interface Response {
  status: Status;
  data?: object;
}

export default class DBResponse {
  private readonly res: Response;

  constructor(errorMessage: string = "Unknown Error") {
    this.res = {
      status: {
        success: false,
        message: errorMessage,
      },
    };
  }

  get response() {
    return this.res;
  }

  /**
   * Update the Response status
   *
   * @param statut
   */
  updateStatus(statut: Status) {
    this.res.status = statut;
  }

  /**
   *
   * Update the status Message
   *
   * @param message
   */
  updateMessage(message: string) {
    this.res.status.message = message;
  }

  /**
   * Update the response data
   *
   * @param data
   */
  updateData(data: object) {
    this.res.data = data;
  }

  /**
   * Update response status and data
   *
   * @param statut
   * @param data
   */
  update(statut: Status, data: object) {
    this.updateStatus(statut);
    this.updateData(data);
  }
}
