/**  ====== DB-Adapter ======
 *
 * Handle the drivers used for database operation
 * - MongoDB uses the Mongoose library
 *
 */

import MongoInstance from "~/plugins/db-adapter/mongoose/export";

export interface DBAdapterInstance {
  connexion: any;
  auth: any;
  utils: any;
  cms: any;
}

const getInstance = async (env: any) => {
  let instance = null;

  // We check for the DB engine specified in the Env Variable
  // And get the corresponding instance (By Default MongoDB Instance)
  switch (env.get("DATABASE_ENGINE")) {
    default:
      instance = MongoInstance;
      break;
  }

  // Connexion
  await instance.connexion.connect(env);

  return instance as DBAdapterInstance;
};

export default {
  getInstance,
};
