/** ===== DB-Adapter Types =====
 *
 * Contains all globally used types
 * used by the DB-Adapter Plugin
 *
 */

import type { HTMLInputType } from "~/types/html-types";

/**
 * Decoded "Auth Token" Json Web Token
 */
export interface SessionAdmin {
  _id: string;
  role: string;
  name: {
    first: string;
    last: string;
  };
  token: string;
}

export interface InputStructure {
  name: string;
  type?: HTMLInputType;
  required?: boolean;
  value?: string | number;
  objProperties?: Array<InputStructure>;
}
