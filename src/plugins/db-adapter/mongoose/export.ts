import MongoConnexion from "./connexion";
import MongoAuth from "./auth";
import MongoUtilities from "./utilities";
import MongoCMS from "./cms-data";

export default {
  connexion: MongoConnexion,
  auth: MongoAuth,
  utils: MongoUtilities,
  cms: MongoCMS,
};
