import mongoose from "mongoose";
import DebugPrint from "~/utils/debug-print";

const connect = async (env: any) => {
  return await mongoose.connect(env.get("DATABASE_URL") as string, {
    user: env.get("DATABASE_USER") as string,
    pass: env.get("DATABASE_PASSWORD") as string,
  });
};

export const debugPrint = new DebugPrint({ prefix: "[DB-Adapter: Mongo]" });

const isConnected = () => {
  const state = mongoose.connection.readyState;
  if (state !== 1) debugPrint.error("Not Connected to Database");
  else debugPrint.success("Connected to Database");
  return state === 1;
};

export default {
  connect,
  isConnected,
};
