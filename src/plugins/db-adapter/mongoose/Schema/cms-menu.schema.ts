import mongoose, { Schema } from "mongoose";
import { IconDefinition } from "@fortawesome/free-regular-svg-icons";

export interface CMSLink {
  label: string;
  slug: string;
  icon?: IconDefinition;
}

export interface CMSCategory {
  label: string;
  menus: Array<CMSLink>;
}

const cmsMenuSchema = new Schema({
  _id: Schema.Types.ObjectId,
  role: String,
  menus: Array<CMSCategory>,
});

const CmsMenu = mongoose.model("cms-menus", cmsMenuSchema);

export default CmsMenu;
