import mongoose, { Schema } from "mongoose";

const adminSchema = new Schema({
  _id: Schema.Types.ObjectId,
  login: String,
  pass: String,
  role: String,
  name: {
    first: String,
    last: String,
  },
  token: String,
});

const Admin = mongoose.model("admins", adminSchema);

export default Admin;
