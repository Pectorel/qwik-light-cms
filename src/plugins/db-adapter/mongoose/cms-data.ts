import CmsMenu from "~/plugins/db-adapter/mongoose/Schema/cms-menu.schema";
import DebugPrint from "~/utils/debug-print";
import type { InputStructure, SessionAdmin } from "~/plugins/db-adapter/types";
import mongoose from "mongoose";

export const debugPrint = new DebugPrint({ prefix: "[DB-Adapter: Mongo]" });

const hiddenFields = ["_id", "modified_at"];

const typesTable = {
  string: "text",
  boolean: "checkbox",
  decimal: "number",
  int: "number",
  date: "date",
  object: "language",
  array: "section",
  default: "text",
};

const getMenu = async (session: SessionAdmin) => {
  debugPrint.info(
    `Getting Menus From cms-menus Collection for ${session.role} role`
  );

  const menus = await CmsMenu.findOne({
    role: session.role,
  });

  if (menus === null) {
    debugPrint.error(
      `No menu found for ${session.role} role. Check your database`
    );
    return null;
  }

  debugPrint.success(`Cms Menus found for ${session.role}!`);
  return menus.menus;
};

/**
 *
 * Returns list of data for given collection name
 *
 * @param collection_name
 * @param env
 */
const find = async (collection_name: string, env: any) => {
  // We get the collection
  const collection = getCollection(collection_name, env);

  const data = collection.find().sort({ _id: -1 });

  const dataArray = [];
  for await (const doc of data) {
    dataArray.push({
      ...doc,
      _id: doc._id.toString(),
    });
  }

  return dataArray;
};

/**
 *
 * Return document with matching _id
 *
 * @param collection_name
 * @param id
 * @param env
 */
const findOne = async (collection_name: string, id: string, env: any) => {
  // We get the collection
  const collection = getCollection(collection_name, env);

  const _id = mongoose.mongo.BSON.ObjectId.createFromHexString(id);

  const row = await collection.findOne({
    _id: _id,
  });

  return {
    ...row,
    _id: row?._id.toString(),
  };
};

/**
 *
 * Return collection from MongoDb Native Client
 *
 * @param collection_name
 * @param env
 */
const getCollection = (collection_name: string, env: any) => {
  // We get the mongoNative Client
  const nativeClient = mongoose.connection.getClient();
  return nativeClient.db(env.get("DATABASE_NAME")).collection(collection_name);
};

/**
 *
 * Insert a new Record inside collection
 *
 * @param collection_name
 * @param env
 * @param data
 */
const insert = async (collection_name: string, data: any, env: any) => {
  // TODO: Rename insert to InsertOne
  const collection = getCollection(collection_name, env);
  return await collection.insertOne(data);
};

/**
 *
 * Update a document with given data
 * Search is based on _id field in data parameter (So it should not be modified !!!)
 *
 * @param collection_name
 * @param env
 * @param data
 */
const update = async (collection_name: string, env: any, data: any) => {
  // TODO : Reverse env and data order in parameters
  const collection = getCollection(collection_name, env);
  const _id = mongoose.mongo.BSON.ObjectId.createFromHexString(data._id);
  delete data._id;

  return await collection.updateOne({ _id: _id }, { $set: data });
};

/**
 *
 * Returns the data structure based on the collection validator
 *
 * @param collection_name
 * @param env
 * @param data
 * @param hidden
 */
const getDataStructure = async (
  collection_name: string,
  env: any,
  data: any = null,
  hidden = hiddenFields
) => {
  debugPrint.info("Getting Collection Structure");
  const collection = getCollection(collection_name, env);

  const options = await collection.options();

  const validator = options.validator["$jsonSchema"];

  if (validator == null) {
    return {
      sucess: false,
      error: {
        errCode: 1,
        errMessage:
          "No Validator for this Collection. Add one so the framework will get the collection structure",
      },
    };
  }

  // TODO: Recursive check
  const structure: Array<InputStructure> = [];

  for (const field in validator.properties) {
    // We don't add Hidden Fields, it will only serve for editing records
    if (!hiddenFields.includes(field)) {
      structure.push(
        generateField(
          field,
          validator.properties[field],
          data,
          validator.required
        )
      );
    }
  }

  // We add all hidden properties as hidden fields for edit
  if (data) {
    for (const key of hidden) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        const fieldStructure: InputStructure = {
          name: key,
          type: "hidden",
          value: data[key],
        };

        structure.push(fieldStructure);
      }
    }
  }

  return structure;
};

const generateField = (
  field: string,
  property: any,
  data: any,
  required: Array<string>
) => {
  // @ts-ignore
  let type = typesTable[property.bsonType];
  type ??= "text";

  const fieldStructure: InputStructure = {
    name: field,
    type: type,
    required: required.includes(field),
  };

  // If data, then add value
  if (data != null) {
    fieldStructure.value = data[field];
  }

  if (type == "section") {
    // Specified data
    fieldStructure.objProperties = [];

    if (Object.prototype.hasOwnProperty.call(property, "items")) {
      const items = property.items;
      for (const childField in items.properties) {
        const childData = data != null ? data[field] : null;
        fieldStructure.objProperties.push(
          generateField(
            childField,
            items.properties[childField],
            childData,
            property.items.required
          )
        );
      }
    }
  }

  return fieldStructure;
};

const deleteOne = async (collection_name: string, id: string, env: any) => {
  const collection = getCollection(collection_name, env);
  const _id = mongoose.mongo.BSON.ObjectId.createFromHexString(id);

  return await collection.deleteOne({ _id: _id });
};

export default {
  getMenu,
  find,
  findOne,
  insert,
  deleteOne,
  update,
  getDataStructure,
};
