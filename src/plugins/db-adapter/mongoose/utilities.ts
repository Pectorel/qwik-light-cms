/** ===== Utilities =====
 *
 * MongoDB utilities functions
 *
 */

import mongoose from "mongoose";

/**
 *
 * Check if Given Collection Exists in DB
 *
 * @param name
 */
const collectionExists = async (name: string) => {
  let res = false;

  const collection = await mongoose.connection.db
    .listCollections({ name: name })
    .toArray();
  if (collection.length > 0) res = true;

  return res;
};

export default {
  collectionExists,
};
