/** ===== Mongo Auth =====
 *
 * Authentification feature for MongoDB
 * Passwords are hashed with bcrypt
 *
 */

import Admin from "~/plugins/db-adapter/mongoose/Schema/admin.schema";
import DBResponse from "~/plugins/db-adapter/db-response";
import MongoUtilities from "~/plugins/db-adapter/mongoose/utilities";
import type { SessionAdmin } from "~/plugins/db-adapter/types";
import bcrypt from "bcrypt";
import DebugPrint from "~/utils/debug-print";
import { generateAuthToken } from "~/plugins/auth/auth";
import type { AdminRow } from "~/plugins/auth/types";

export interface Credential {
  login: string;
  password: string;
}

export interface AuthenticatedUser {
  token: string;
}

export const debugPrint = new DebugPrint({ prefix: "[DB-Adapter: Mongo]" });

// TODO : Add Refresh Token
const auth = async (credentials: Credential, env: any) => {
  debugPrint.info("Trying to Auth Admin in Mongoose");
  const res = new DBResponse();

  res.updateMessage("Credentials do not match our records");

  // Checking if collection exists
  const exists = await MongoUtilities.collectionExists("admins");

  if (!exists) {
    debugPrint.error("Admin Collection does not exists in DB");
    res.updateMessage(
      "Admins collection does not exists in current database. \n Did you run the qwik-light-cms install process?"
    );
    return res;
  }

  debugPrint.success("Admin Collection exists in DB");

  // Checking if User exists
  const cond = {
    login: credentials.login,
  };

  const admin = await Admin.findOne(cond);

  // Checking for user records and if password matches hash
  if (
    admin == null ||
    !(await bcrypt.compare(credentials.password, admin.pass as string))
  ) {
    debugPrint.error("Credentials does not match DB Records");
    return res;
  }

  // If credentials are matching DB records
  debugPrint.success("Credentials match an Admin Record in DB! Yay !");

  const tokens = generateAuthToken(admin as unknown as AdminRow, env);
  // Setting SessionToken to admin row and save in DB
  admin.token = tokens.sessionToken;
  await admin.save();
  debugPrint.success("Auth Token Created and saved in DB");

  // Setting JWT Token to store on client
  const authenticatedUser: AuthenticatedUser = {
    token: tokens.jwtToken as string,
  };

  // Setting Response with UserData
  const status = {
    success: true,
    message:
      "Successfully Logged In! You are being redirected to your Dashboard",
  };
  res.update(status, {
    admin: authenticatedUser,
  });

  debugPrint.success("Returning Response with generated JWT");

  return res;
};

const checkSession = async (sessionData: SessionAdmin) => {
  return Admin.findOne({
    role: sessionData.role,
    token: sessionData.token,
  });
};

export default {
  auth,
  checkSession,
};
