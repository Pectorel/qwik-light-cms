import { defineConfig } from "@pandacss/dev";
import { wrapperRecipe } from "~/pandacss/recipes/config/wrapper.recipe";

export default defineConfig({
  jsxFramework: "qwik",

  // Whether to use css reset
  preflight: true,

  // Where to look for your css declarations
  include: ["./src/**/*.{js,jsx,ts,tsx}"],

  // Files to exclude
  exclude: [],

  // Useful for theme customization
  theme: {
    extend: {
      recipes: {
        wrapper: wrapperRecipe,
      },
    },
    tokens: {
      colors: {
        primary: { value: "#ac7ef4" },
        secondary: { value: "#18b6f6" },
        blue: {
          100: { value: "#09a0dd" },
          200: { value: "#0896d0" },
        },
        purple: {
          50: { value: "#b48bf5" },
          100: { value: "#782eed" },
          150: { value: "#6514e6" },
          200: { value: "#5611c2" },
          300: { value: "#5812c8" },
          400: { value: "#4b0faa" },
          500: { value: "#3f3b6c" },
          600: { value: "#3c0c88" },
        },
        grey: {
          100: { value: "#e5e5e5" },
        },
      },
    },
  },

  // The output directory for your css system
  outdir: "styled-system",
});
